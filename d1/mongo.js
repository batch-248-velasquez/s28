// CRUD Operations

/*
	CRUD operations are the heart of any backend application

*/

//Insert Documents (Create)

//Inserting one document

	//syntax
		//db.collectionName.insertOne ({object});
		//db.collectionName.insert({object});

	//Javascript syntax comparison
		//object.object.method({object});


	db.users.insert({
	    firstName: "Jane",
	    lastName: "Doe",
	    age: 21,
	    contact: {
	        phone: "87654321",
	        email: "janedoe@gmail.com"
	    },
	    courses: ["CSS", "Javascript", "Python"],
	    department: "none"
	});

	//Insert Many

	//Syntax
		//db.collectionName.insertMany ([{obectA},{objectB}]);

	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76
			contact: {
				phone: "8765421",
				email: "stephenhawk@gmail.com"
			},
			courses: ["python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82
			contact: {
				phone: "8765421",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	]);





	//Finding Documents (Read/Retrieve)

	//Find
	/*
		if multiple documents macth the criteria for finding a document only the FIRST docuemnt that matches the term will be returned

		-This is also based from the order that the document are stored in the collections
		-If the document is not found, the terminal will respond with a blank line


		Syntax:
		-db.collenctionName.find()
		-db.collectionName.find({field.value});

	*/

	//Finding a single document
	// leaving the search criteria empty will retrieve ALL documents

	db.users.find()

	db.users.find({firstName: "Stephen"});


	//Finding documents with multiple parameters

	/*
		syntax:
			db.collectionName.find({fieldA: valueA, fieldB: valueB});


	*/


		db.users.find({lastName: "Armstrong", Age: 82});


		//Updating documents

		//update a singe document

		//***********create a document that we will update

		db.users.insert({
			firstName: "test",
			lastName: "Test",
			age: 0,
			contact: {
				phone: "0000000",
				email: "test@gmail.com"
			},
			courses: [],
			department: "none"

		});

		/*
			Just like the "find" method, mthods that only manip[ulate a singe document will only update the FIRST document that matches the searcvh criteria

			syntax:
			db.collectionsName.updateOne({criteria}, {$set:{field:vlaue}})

		*/


		db.users.updateOne(
			{firstName:"test"}, 
			{
				$set: {
					firstName: "Bill",
					lastName: "Gates",
					age: 65,
					contact: {
						phone: "12364572354",
						email: "bill@gmail.com"
					},
					courses: ["PHP", "Laravel", "HTML"],
					department: "operations",
					status: "active"
				}

			}

		);


		//update multiple documents

		/*
			syntax:
			db.collectionName.updateMany({criteria}, {$set: {field:value}});


			)

		*/



			db.users.updateMany(
				{department:"none"},
				{
					$set: {department: "HR"}
				}
			);


		//replaceone

	/*
		Can e used if replacing the whole document is necessary
		syntax:
			db.collectionName.replaceOne({criteria}, {field:value});

	*/

			db.users.replaceOne(
				{firstName: "Bill"},
				{
					firstName: "Bill",
					lastName: "Gates",
					age: 65,
					contact: {
						phone: "123456789",
						email: "bill@gmail.com"
					},
					courses: ["PHP", "Laravel", "HTML"],
					department: "operations"
				}
			)


			//deleting documents (delete)

			//creating a document to delete

			db.users.insert({
				firstName: "test"

			});


			//delete a single document
			/*

				syntax:
					db.collectionName.deleteOne ({criteria});

			*/

				db.users.deleteOne ({

					firstName: "test"
				});

			//delete many

			/*
			Syntax: 
				db.collectionName.deleteMany ({criteria});
			*/


				db.users.deleteMany({
					firstName: "Bill"
				});


			//Advance Queries

			//Query an embedded document

				db.users.find({
					contact:{
						phone: "8765421",
						email: "stephenhawk@gmail.com"
					}
				});


			//query on nested field
				
				db.users.find(
					{"contact.email": "janedoe@gmail.com"}
				)

				//query an array with exact elements

				db.users.find({courses: ["CSS", "Javascript", "python"]})


				//querying an array without regard to order
				db.users.find ({courses: {$all: ["React", "Python"] } })


				//querying an embedded array
				//added another document
				db.users.insert({
					namearr: [
						{	
							namea: "Juan"
							},
						{
							nameb: "Tamad"
						}	
						]
				})

				db.users.find ({
					namearr:
					{

						namea: "Juan"
					}

					})